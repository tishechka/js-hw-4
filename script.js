/*
1) Описати своїми словами навіщо потрібні функції у програмуванні.

Якщо кілька разів у різних місцях коду потрібно виконати одну й ту саму або схожу послідовність команд,
можна винести їх в окрему функцію і викликати її, тим самим значно зменшивши кількість коду, яку потрібно буде написати.
Функції плегшують роботу програмістів та роблять код більш організованим та зрозумілим.

2) Описати своїми словами, навіщо у функцію передавати аргумент.

Аргументи - значення або змінні, які передаються в функцію під час її виклику.
Це дозволяє функції отримувати зовнішні дані і використовувати їх для виконання певних операцій або обчислень.

3) Що таке оператор return та як він працює всередині функції?

Оператор return використовується в програмуванні для повернення значень з функції.
Він вказує на закінчення виконання функції та передає результат обчислення назовні, де функція була викликана.
*/



let num1 = ""
let num2 = ""

do {
    num1 = +prompt("Введіть перше число", num1);
    num2 = +prompt("Введіть друге число", num2);
} while (isNaN(num1) && isNaN(num2));


let calc = prompt("Оберіть математичну операцію +, -, *, /, **");

while (calc !== "+" &&
calc !== "-" &&
calc !== "*" &&
calc !== "/" &&
calc !== "**") {
    calc = prompt("Оберіть математичну операцію +, -, *, /, **");
}

function compute(num1, num2, calc) {
    let result;

    switch (calc) {
        case '+':
            result = num1 + num2;
            break;
        case "-":
            result = num1 - num2;
            break;
        case "*":
            result = num1 * num2;
            break;
        case "/":
            result = num1 / num2;
            break;
        case "**":
            result = num1 ** num2
            break;
    }
    return result;
}

const result = compute(num1, num2, calc);
console.log(result);

